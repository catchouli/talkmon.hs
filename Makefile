all:
	cd client && stack build
	cd server && stack build

install: all
	cd server && stack build --copy-bins

run: all
	cd server && stack exec talkmon -- --port 13003
