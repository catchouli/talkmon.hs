//'use strict';

a = {x:5};

/// Game class
function Game(io) {
  this.io = io;
  this.phaser = new Phaser.Game( 600, 600, Phaser.AUTO, ''
                               , { preload: this.preload.bind(this)
                                 , create:  this.create.bind(this)
                                 , update:  this.update.bind(this)
                                 , render:  this.render.bind(this)
                                 }
                               );
}

/// Preload resources
Game.prototype.preload = function() {
  let phaser = this.phaser;

  // Load sprites
  phaser.load.image('patchouli', 'assets/sprites/patchouli.png');
  phaser.load.spritesheet('leafgreen_girl_walk', 'assets/sprites/characters/leafgreen_girl_walk.png', 30, 38);
  phaser.load.spritesheet('leafgreen_girl_run', 'assets/sprites/characters/leafgreen_girl_run.png', 32, 38);

  // Load music
  phaser.load.audio('bgm-palette-town', 'assets/sound/102-palette-town-theme.mp3');
};

/// Create objects
Game.prototype.create = function() {
  let phaser = this.phaser;
  let physics = phaser.physics;

  // Create input
  this.inputStreams = this.createInputStreams();

  // Create player
  this.localPlayer = new Character(this);

  // Create bgm audio player
  this.bgm = phaser.add.audio('bgm-palette-town');
  this.bgm.volume = 0.25;
  //this.bgm.play();

  // Connect to server
  this.io.on("enter_world", console.log);
  this.io.emit("enter_world");
};

Game.prototype.createInputStreams = function() {
  let keyboard = this.phaser.input.keyboard;

  // Define input streams
  // The events in this map should be abstracted from the actual input
  // i.e. the event should be called 'left', not 'leftKey', and it should
  // be triggered by any left inputs
  return {
    tick: (() => {
      let tick = Kefir.stream(e => { this.emitTick = e; });
      // Needed to let the stream activate and therefore initialise this.emitTick
      tick.observe(() => {});
      return tick;
    })(),
    left: Kefir.stream(emitter => {
      let leftKey = keyboard.addKey(Phaser.KeyCode.LEFT);
      emitter.emit(false);
      leftKey.onDown.add(_ => emitter.emit(true));
      leftKey.onUp.add(_ => emitter.emit(false));
    }),
    right: Kefir.stream(emitter => {
      let rightKey = keyboard.addKey(Phaser.KeyCode.RIGHT);
      emitter.emit(false);
      rightKey.onDown.add(_ => emitter.emit(true));
      rightKey.onUp.add(_ => emitter.emit(false));
    }),
    up: Kefir.stream(emitter => {
      let upKey = keyboard.addKey(Phaser.KeyCode.UP);
      emitter.emit(false);
      upKey.onDown.add(_ => emitter.emit(true));
      upKey.onUp.add(_ => emitter.emit(false));
    }),
    down: Kefir.stream(emitter => {
      let downKey = keyboard.addKey(Phaser.KeyCode.DOWN);
      emitter.emit(false);
      downKey.onDown.add(_ => emitter.emit(true));
      downKey.onUp.add(_ => emitter.emit(false));
    }),
    buttonA: Kefir.stream(emitter => {
      let zKey = keyboard.addKey(Phaser.KeyCode.Z);
      emitter.emit(false);
      zKey.onDown.add(_ => emitter.emit(true));
      zKey.onUp.add(_ => emitter.emit(false));
    }),
    buttonB: Kefir.stream(emitter => {
      let xKey = keyboard.addKey(Phaser.KeyCode.X);
      emitter.emit(false);
      xKey.onDown.add(_ => emitter.emit(true));
      xKey.onUp.add(_ => emitter.emit(false));
    }),
  };
};

/// Update callback
Game.prototype.update = function() {
  this.emitTick.emit(this.phaser.time.physicsElapsed);
};

/// Render callback
Game.prototype.render = function() {
  this.phaser.debug.inputInfo(32, 32);
};
