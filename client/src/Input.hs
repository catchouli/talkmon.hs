{-# LANGUAGE TemplateHaskell #-}

module Input
  ( InputSources
  , initInput
  , left
  , right
  , up
  , down
  , spacebar
  )
where

import GHCJS.Types (JSVal)
import Control.Lens ((^.), makeLenses)
import Language.Javascript.JSaddle (js, jsg, jsf, fun)
import Reactive.Banana.Frameworks (AddHandler, newAddHandler)


-- | Input sources
data InputSources = InputSources { _left :: AddHandler Bool
                                 , _right :: AddHandler Bool
                                 , _up :: AddHandler Bool
                                 , _down :: AddHandler Bool
                                 , _spacebar :: AddHandler Bool
                                 }
makeLenses ''InputSources


-- | Set up input signals
initInput :: JSVal -> IO InputSources
initInput p = InputSources <$> addKeyHandler p "LEFT"
                           <*> addKeyHandler p "RIGHT"
                           <*> addKeyHandler p "UP"
                           <*> addKeyHandler p "DOWN"
                           <*> addKeyHandler p "SPACEBAR"


-- | Adds a key handler to phaser and returns an event
addKeyHandler :: JSVal -> String -> IO (AddHandler Bool)
addKeyHandler phaser name = do
  let cb = const . const . const
  let key name = jsg "Phaser" ^. js "KeyCode" ^. js name
  let addKey name = phaser ^. js "input" ^. js "keyboard" ^. jsf "addKey" [key name]

  (handler, sink) <- newAddHandler
  key <- addKey name
  key ^. js "onDown" ^. jsf "add" [fun . cb $ sink True]
  key ^. js "onUp" ^. jsf "add" [fun . cb $ sink False]

  return handler

