{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts #-}

module Main where

import Input

import Control.Lens ((^.), makeLenses, to)
import Control.Monad (void, filterM, (>=>), liftM)
import Language.Javascript.JSaddle ( js, jsg, jss, jsf, new, obj, fun, val
                                   , array, valToText, strictEqual, (<#))
import qualified JavaScript.Array as Array
import GHCJS.Types (JSVal(..), isUndefined)
import GHCJS.Marshal ()
import GHCJS.Foreign ()
-- import JavaScript.JSON ()
import Control.Monad.IO.Class
import System.Random
import qualified Data.Aeson as Aeson

import Data.IORef

import Prelude hiding ((.))

import FRP.Netwire
import Control.Wire


-- A wire state, with (previous value, wire, session)
type GS s e m a b = (Either e b, Wire s e m a b, Session m s)


-- | A phaser callback. Takes a phaser instance
type PhaserCallback = JSVal -> IO ()


-- The fixed timestep
timestep :: Float
timestep = 1 / 60


-- | Load data
preload :: JSVal -> IO ()
preload phaser = void $ do
  let loadImage name url = (js "load" . jsf "image" [name, url])
  let loadSpritesheet name url w h = (js "load" . jsf "spritesheet" [val name, val url, val (w::Int), val (h::Int)])

  phaser ^. loadImage "patchouli" "assets/sprites/patchouli.png"
  phaser ^. loadSpritesheet "leafgreen_girl_walk" "assets/sprites/characters/leafgreen_girl_walk.png" 30 38
  phaser ^. loadSpritesheet "leafgreen_girl_run" "assets/sprites/characters/leafgreen_girl_run.png" 32 38

  return ()


-- | Create game
create :: JSVal -> IO ()
create phaser = do
  let add = (+)
  let sub = subtract
  let addSprite x y image = (js "add" . jsf "sprite" [val (x::Int), val (y::Int), val image])
  let setAnchor f = (js "anchor" . jsf "set" [(f::Float)])
  let addAnimation name frames = (js "animations" . jsf "add" [val name, val (frames::[Int])])
  let playAnimation name framerate loop = (js "animations" . jsf "play" [val name, val (framerate::Int), val (loop::Bool)])

  phaser ^. js "scale" ^. jss "scaleMode" (jsg "Phaser" ^. js "ScaleManager" ^. js "RESIZE")

  playerSpriteJS <- phaser ^. addSprite 0 0 "leafgreen_girl_walk"
  playerSpriteJS ^. setAnchor 0.5
  playerSpriteJS ^. addAnimation "walk_down" [0,1,2]
  playerSpriteJS ^. addAnimation "walk_up"   [3,4,5]
  playerSpriteJS ^. addAnimation "walk_left" [6,7,8]
  playerSpriteJS ^. playAnimation "walk_down" 0 True

  return ()


-- | Update game
update :: JSVal -> IORef (GS s e IO a b) -> IO ()
update phaser state = do
  -- todo: make sure we update at 'timestep'
  step state


-- | Render game
render :: JSVal -> IORef (GS s () IO a Float) -> IO ()
render phaser state = do
  (v, _, _) <- readIORef $ state
  print $ v
  return ()


-- | Step a wire state stored in an ioref
step :: IORef (GS s e IO a b) -> IO ()
step a = do
  let step' (wire, session) = do
        (s, session') <- stepSession session
        (w, wire') <- stepWire wire s (Right undefined)
        return  (w, (wire', session'))
  (_, v, g) <- readIORef a
  (w, b) <- step' (v,g)
  writeIORef a (w, fst b, snd b)


-- | Create game
main :: IO ()
main = do
  state <- newIORef (undefined, time, countSession_ timestep)

  -- Initialise phaser
  phaser <- new ( jsg "Phaser" ^. js "Game" )
                ( 800 :: Int
                , 600 :: Int
                , jsg "Phaser" ^. js "AUTO"
                , ""
                , obj >>= \cb -> do
                    cb <# "preload" $ fun (\_ _ [p] -> preload p)
                    cb <# "create"  $ fun (\_ _ [p] -> create p)
                    cb <# "update"  $ fun (\_ _ [p] -> update p state)
                    cb <# "render"  $ fun (\_ _ [p] -> render p state)
                    return cb
                )

  -- Store reference in document
  doc <- jsg "document"
  doc ^. jss "phaser" phaser
