{-# LANGUAGE Arrows #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Arrow where

import Prelude hiding ((.), id)
import Control.Category ((.))
import FRP.Netwire
import System.Random
import Control.Wire
import Control.Wire.Core
import Control.Wire.Event
import Control.Wire.Unsafe.Event
import Control.Arrow
import Control.Monad.IO.Class
import Control.Monad (replicateM, liftM)
import Control.Monad.State
import Control.Monad.Reader
import Data.IORef
import qualified Data.Map as M
import System.IO.Unsafe


data Sprite = Sprite { spriteId :: Int } deriving (Show)


-- | A mortal sin.
spriteIds :: IORef Int
spriteIds = unsafePerformIO $ newIORef 0


-- | From an initial value and a wire producing said value and an action, create
-- a property wire that runs said action whenever the wire changes. Operates on
-- a passthrough value from another wire for holding object references.
prop :: (Monoid e, Show e, Show b, Eq e, Eq b, MonadIO m, HasTime t s, Fractional t)
           => b
           -> (Sprite -> b -> IO ())
           -> Wire s e m a b
           -> Wire s e m Sprite Sprite
prop init update wire =
  let manage prev = mkGen $ \s input@(Sprite spriteId) -> do
                    let ifM a b = if a then b else return ()
                    let (old, w) = prev
                    (v, w') <- stepWire w s (Right undefined)
                    case v of
                      Left _ -> return ()
                      Right newVal -> ifM (old /= v) $ liftIO $ update input newVal
                    case v of
                         Left v -> return (Left v, mkEmpty)
                         Right _ -> return (Right input, manage (v, w'))
  in manage (Right init, wire)


-- | Create a new sprite with a global reference.
newSprite :: (HasTime t s, Fractional t)
          => Wire s () IO a Sprite
newSprite = let spriteGen = mkGen $ \s input -> do
                  spriteId <- readIORef spriteIds
                  putStrLn $ "creating sprite " ++ show spriteId
                  modifyIORef spriteIds (+1)
                  return (Right (Sprite spriteId), pure (Sprite spriteId))
            in spriteGen


-- | Update a sprite's position
updateSpritePos (Sprite spriteId) pos = putStrLn $ "updating sprite " ++ show spriteId ++ " position to " ++ show pos


-- | Deletes a sprite reference.
deleteSprite (Sprite spriteId) = putStrLn $ "deleting sprite " ++ show spriteId


-- | Perform a monadic action when a given event containing a list is emited, updating each value.
-- Currently does nothing but I thought it might be useful.
newWire :: (Monad m, Traversable t)
        => (a -> m b)
        -> Wire s e m (Event (t a)) (Event (t b))
newWire action = onEventM (mapM action)


-- | Evoke an IO action when a sprite starts inhibiting.
-- If the wire starts out inhibiting, does nothing.
onInhibit :: (MonadIO m, HasTime t s, Fractional t)
          => (b -> IO ())
          -> Wire s () m a b
          -> Wire s () m a b
onInhibit action wire =
  let manage prev = mkGen $ \s _ -> do
        let (oldVal, w) = prev
        (iv, w') <- stepWire w s (Right undefined)
        case iv of
             Left _ -> do
               case oldVal of
                    Just a -> liftIO $ action a
                    Nothing -> return ()
               return (iv, w')
             Right v -> return (iv, manage (Just v, w'))
  in manage (Nothing, wire)


-- | Collects event occurences of wires into a collection of wires
-- | Possible todo: as well as accumulating wire creations, accumulate deletions too
collectWires :: (Monad m, MonadIO m, HasTime t s, Fractional t)
    => (b -> IO ()) -> Wire s () m (Event [Wire s () m a b]) [b]
collectWires deleter =
  let manage ws = mkGen $ \s input -> do
        a <- mapM (\w -> stepWire w s (Right undefined)) ws
        let alive = [ (r, w) | (Right r, w) <- a ]
        let vals = Right (map fst alive)
        let wire' = manage (map snd alive ++ fmap (onInhibit deleter) input)
        return (vals, wire')
      pulseWire =
        (rSwitch mkEmpty) .
        (mkId &&& (mkSF_ $ fmap (\x -> mkSFN (\_ -> (x, pulseWire)))))
      pollEvents = proc a -> do
                     pulseWire -< a
                   <|> do
                     returnA -< []
  in manage [] . pollEvents


-- | Integrate a 2d vector
integrate2 :: (Monad m, HasTime t s, Fractional t) => Wire s () m (Float, Float) (Float, Float)
integrate2 = proc (x,y) -> do
                  x' <- integral 0 -< x
                  y' <- integral 0 -< y
                  returnA -< (x',y')


-- | Velocity of a sprite
vel :: (Monad m, HasTime t s, Fractional t) => Wire s () m a (Float, Float)
vel = proc _ -> do for 0.001 . pure (1,1)     -< ()
            -- --> do for 1 . pure (0,0)    -< ()
            -- --> do for 1 . pure (0,-1)    -< ()
            -- --> do for 1 . pure (-1,-0.1) -< ()
            -- --> do for 1 . pure (0.5,1)   -< ()
            -- --> do for 1 . pure (0,0)     -< ()
            -- --> do vel -< ()


-- | Position of a sprite
pos :: (Monad m, HasTime t s, Fractional t) => Wire s () m a (Float, Float)
pos = integrate2 . vel


-- | Generates sprites every second
spriteGen :: (HasTime t s, Fractional t) => Wire s () IO a (Event [Wire s () IO a Sprite])
spriteGen = periodic 1 . pure [mySprite]


-- | Sample a wire in a loop, printing the value. Runs in IO.
sampleLoop wire = do
  a <- newIORef (wire, clockSession_)
  let loop = do
        w <- stepIO a
        case w of
             Left _ -> return ()
             Right w -> print w
        loop 
  loop

-- | Step a wire and session.
step :: Monad m
     => (Wire s e m a b, Session m s)
     -> m (Either e b, (Wire s e m a b, Session m s))
step (wire, session) = do
  (s, session') <- stepSession session
  (w, wire') <- stepWire wire s (Right undefined)
  return  (w, (wire', session'))


-- | Step a wire and session stored in an IORef
stepIO :: IORef (Wire s e IO a b, Session IO s) -> IO (Either e b)
stepIO a = do
  v <- readIORef a
  (w, b) <- step v
  writeIORef a b
  return w


-- | A sample sprite.
mySprite :: (HasTime t s, Fractional t) => Wire s () IO a Sprite
mySprite = onInhibit deleteSprite $
            newSprite
              >>> prop (0,0) updateSpritePos pos


-- | Test with a sprite generator
s = do
  spriteIds <- newIORef (0::Int)
  sampleLoop $ collectWires deleteSprite . spriteGen
