{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Server
import System.Console.CmdArgs
import Paths_talkmon (getDataDir)

import qualified Snap.Core as Snap
import qualified Snap.Http.Server as Snap
import qualified Snap.Util.FileServe as Snap
import qualified Network.SocketIO as SocketIO
import qualified Network.EngineIO.Snap as EIOSnap


-- | Command line arguments
data CommandArgs = CommandArgs { port :: Int } deriving (Show, Data, Typeable)


-- | Command line argument parser
argParser = CommandArgs { port = 13000 &= help "Port to bind to" }
              &= summary "Talkmon v0.0.0, (C) Caitlin Wilks"
              &= program "talkmon"


-- | Start the server with the specified config
start :: CommandArgs -> FilePath -> IO ()
start args resourceDir = do
  socketIoHandler <- SocketIO.initialize EIOSnap.snapAPI server
  dataDir <- getDataDir
  let config = (flip mconcat) mempty [ Snap.setPort (port args) ]
  Snap.simpleHttpServe (config :: Snap.Config (Snap.Snap) a) $
    Snap.route [ ("/socket.io", socketIoHandler)
               , ("/", Snap.serveDirectory resourceDir)
               ]


-- | Main uses the installed resources to start the server
-- For GHCI use use 'test' instead and make sure you're in the correct directory..
main :: IO ()
main = do
  args <- cmdArgs argParser
  dataDir <- getDataDir
  start args dataDir


-- | Test uses the 'resources' folder in the current directory
test :: IO ()
test = start (CommandArgs { port = 13000 }) "../client/resources"
