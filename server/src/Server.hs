{-# LANGUAGE OverloadedStrings #-}

module Server where

import Control.Applicative
import Data.Aeson ((.=), (.:))
import Control.Monad.Trans.Reader
import qualified Snap.Core as Snap
import qualified Data.Aeson as Aeson
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.State.Strict
import qualified Network.SocketIO as SocketIO


-- | Message packet, encodes a string
data Message = Message String String

instance Aeson.FromJSON Message where
  parseJSON = Aeson.withObject "message" $ \o -> Message <$> o .: "name" <*> o .: "message"

instance Aeson.ToJSON Message where
  toJSON (Message n m) = Aeson.object [ "name" .= n, "message" .= m ]


-- | Our socket.io server declaration
server :: StateT SocketIO.RoutingTable (ReaderT SocketIO.Socket Snap.Snap) ()
server = do
  SocketIO.on "enter_world" $ SocketIO.emit "enter_world" $ Message "a" "b"
  SocketIO.on "message" $ \(Message name message) -> (liftIO . putStrLn $ name ++ ": " ++ message) >> (SocketIO.emit "message" $ Message "a" "b")
